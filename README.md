A web scrapper for downloading ACARA JSON manifests.

To run, the app you will need to give deno read/write access to the filesystem. You can run the app by running the following command.

```bash
deno run --allow-net --allow-write --allow-read app.ts
```
or

```
deno task run
```

To compile the app to run you can use the deno compile command.

```bash
deno compile --allow-net --allow-write --allow-read app.ts
```
or

```
deno task build
```

download the repo and compile the app using deno's compile command.
